export const getDifferenceValues = (startDate, endDate) => {
    const monthDifference = endDate.getMonth() - startDate.getMonth()
    const yearsDifference = endDate.getFullYear() - startDate.getFullYear()

    const period = yearsDifference * 12 + monthDifference

    return [Math.floor(period / 12), period % 12]
}

export const getMonthesText = (amount) => {
    return amount === 1
        ? "месяц"
        : amount > 1 && amount < 5
        ? "месяца"
        : "месяцев"
}

export const getYearsText = (amount) => {
    if (amount < 21) {
        return amount === 1 ? "год" : amount > 1 && amount < 5 ? "года" : "лет"
    }
    const remain = amount % 10
    return remain === 1 ? "год" : remain > 1 && remain < 5 ? "года" : "лет"
}
