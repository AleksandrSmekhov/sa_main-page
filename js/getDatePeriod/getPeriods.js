import { getDifferenceValues, getMonthesText, getYearsText } from "./utils.js"

export const insertPeriod = (idName, startDateString, endDateString = "") => {
    const startDate = new Date(startDateString)
    const endDate = endDateString ? new Date(endDateString) : new Date()

    const freelancePeriod = document.getElementById(idName)

    const [freelanceYears, freelanceMonthes] = getDifferenceValues(
        startDate,
        endDate
    )

    if (freelancePeriod) {
        freelancePeriod.prepend(
            `${startDate.toLocaleDateString().slice(3)} - ${
                endDateString ? endDate.toLocaleDateString().slice(3) : "н.в."
            }, ${freelanceYears} ${getYearsText(
                freelanceYears
            )} и ${freelanceMonthes} ${getMonthesText(freelanceMonthes)}`
        )
    }
}

export const insertAge = (idName, birthDateString) => {
    const birthDate = new Date(birthDateString)
    const currentDate = new Date()

    const age = document.getElementById(idName)

    const [ageYears, _] = getDifferenceValues(birthDate, currentDate)

    if (age) {
        age.prepend(`${ageYears} ${getYearsText(ageYears)}`)
    }
}
