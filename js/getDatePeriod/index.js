import { insertPeriod, insertAge } from "./getPeriods.js"

const BIRTH_ID = "place-age-age"
const BIRTH_DATE = "1998-07-09T08:00:00.000Z"

const FREELANCE_WORK_ID = "freelance-work-period"
const FREELANCE_START_DATE = "2018-02-01T00:00:00.000Z"

const AIB_WORK_ID = "aib-work-period"
const AIB_START_DATE = "2023-03-01T00:00:00.000Z"
const AIB_END_DATE = "2024-07-01T00:00:00.000Z"

insertPeriod(FREELANCE_WORK_ID, FREELANCE_START_DATE)
insertPeriod(AIB_WORK_ID, AIB_START_DATE, AIB_END_DATE)
insertAge(BIRTH_ID, BIRTH_DATE)
